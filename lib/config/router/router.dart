import 'package:go_router/go_router.dart';
import 'package:push_app/features/push/presentation/screens/detail/detail_screen.dart';
import 'package:push_app/features/push/presentation/screens/home/home_screen.dart';

final GoRouter router = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      builder: (_, state) => const HomeScreen(),
    ),
    GoRoute(
      path: '/push-detail/:messageId',
      builder: (context, state) =>
          DetailScreen(messageId: state.pathParameters['messageId'] ?? ''),
    )
  ],
);
