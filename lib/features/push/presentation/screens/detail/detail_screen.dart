import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:push_app/features/push/domain/entities/push_entity.dart';
import 'package:push_app/features/push/presentation/blocs/notifications/notifications_bloc.dart';

class DetailScreen extends StatelessWidget {
  final String messageId;

  const DetailScreen({super.key, required this.messageId});

  @override
  Widget build(BuildContext context) {
    final PushEntity? message =
        context.watch<NotificationsBloc>().getPushById(messageId);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail'),
      ),
      body: (message != null)
          ? _DetailView(message: message)
          : const Center(
              child: Text('Notification not exist'),
            ),
    );
  }
}

class _DetailView extends StatelessWidget {
  final PushEntity message;

  const _DetailView({required this.message});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 20,
      ),
      child: Column(
        children: [
          if (message.imageUrl != null) Image.network(message.imageUrl!),
          const SizedBox(height: 30),
          Text(message.title),
          Text(message.body),
          const Divider(),
          Text(message.data.toString()),
        ],
      ),
    );
  }
}
